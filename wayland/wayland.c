#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <linux/input-event-codes.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wayland-client-protocol.h>
#include <wayland-util.h>

#include "wayland.h"

#include "xdg-shell/xdg-shell.h"
#include "xdg-shell/xdg-shell-client-protocol/xdg-shell-client-protocol.h"

static void listenRegistryGlobal(void *data, struct wl_registry *wl_registry, uint32_t name, const char *interface,
	uint32_t version);
static void listenRegistryGlobalRemove(void *data, struct wl_registry *wl_registry, uint32_t name);
static void listenDisplayListenerError(void *data, struct wl_display *wl_display, void *object_id, uint32_t code,
	const char *message);
static void listenDisplayListenerDeleteId(void *data, struct wl_display *wl_display, uint32_t id);
static void listenSurfaceEnter(void *data, struct wl_surface *wl_surface, struct wl_output *output);
static void listenSurfaceLeave(void *data, struct wl_surface *wl_surface, struct wl_output *output);

static struct wl_compositor *compositor;
static XdgShell *shell;

static struct wl_display_listener displayListener = {
	.error = listenDisplayListenerError,
	.delete_id = listenDisplayListenerDeleteId
};
static struct wl_registry_listener registryListener = {
	.global = listenRegistryGlobal,
	.global_remove = listenRegistryGlobalRemove
};
static struct wl_surface_listener surfaceListener = {
	.enter = listenSurfaceEnter,
	.leave = listenSurfaceLeave
};

Wayland *Wayland_Create(int32_t width, int32_t height, const char *title)
{
	Wayland *wayland = NULL;

	wayland = (Wayland *)malloc(sizeof (Wayland));
	assert(wayland);
	wayland->display = wl_display_connect(NULL);
	assert(wayland->display);
	// wl_display_add_listener(wayland->display, &displayListener, NULL);

	wayland->registry = wl_display_get_registry(wayland->display);
	wl_registry_add_listener(wayland->registry, &registryListener, wayland);
	wl_display_roundtrip(wayland->display);

	wayland->surface = wl_compositor_create_surface(compositor);
	assert(wayland->surface);
	wl_surface_add_listener(wayland->surface, &surfaceListener, wayland);

	int32_t size = width * height * 4;
	int32_t stride = width * 4;
	int fd = shm_open("jpeg-renderer", O_RDWR | O_CREAT | O_EXCL, S_IWUSR | S_IRUSR | S_IWOTH | S_IROTH);
	shm_unlink("jpeg-renderer");
	ftruncate(fd, size);
	
	shell = XdgShell_Create(wayland->display, wayland->surface, title);
	wl_surface_commit(wayland->surface);

	return wayland;
}

void Wayland_Destroy(Wayland *wayland)
{
	wl_surface_destroy(wayland->surface);
	wl_registry_destroy(wayland->registry);
	wl_display_disconnect(wayland->display);
	free(wayland);
}

static void listenRegistryGlobal(void *data, struct wl_registry *wl_registry, uint32_t name, const char *interface,
	uint32_t version)
{
	Wayland *wayland = (Wayland *)data;

	if (strcmp(interface, wl_compositor_interface.name) == 0) {
		compositor = wl_registry_bind(wl_registry, name, &wl_compositor_interface, version);
	}
	printf("interface: %s | name: %d\n", interface, name);
}

static void listenRegistryGlobalRemove(void *data, struct wl_registry *wl_registry, uint32_t name)
{}

static void listenSurfaceEnter(void *data, struct wl_surface *wl_surface, struct wl_output *output)
{}

static void listenSurfaceLeave(void *data, struct wl_surface *wl_surface, struct wl_output *output)
{}

static void listenDisplayListenerError(void *data, struct wl_display *wl_display, void *object_id, uint32_t code,
	const char *message)
{}

static void listenDisplayListenerDeleteId(void *data, struct wl_display *wl_display, uint32_t id)
{}

static void listenBufferRelease(void *data, struct wl_buffer *wl_buffer)
{}
