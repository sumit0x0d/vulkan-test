#ifndef VULKAN_TEST_WAYLAND_H
#define VULKAN_TEST_WAYLAND_H

#include <stdint.h>

#include <wayland-client.h>
#include <wayland-cursor.h>

typedef struct {
	struct wl_display *display;
	struct wl_registry *registry;
	struct wl_surface *surface;
} Wayland;

Wayland *Wayland_Create(int32_t width, int32_t height, const char *title);
void Wayland_Destroy(Wayland *wayland);

#endif
