#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-client-protocol.h>

#include "xdg-shell.h"

#include "xdg-shell-client-protocol/xdg-shell-client-protocol.h"
	
static void listenRegistryGlobal(void *data, struct wl_registry *wl_registry, uint32_t name, const char *interface,
	uint32_t version);
static void listenRegistryGlobalRemove(void *data, struct wl_registry *wl_registry, uint32_t name);
static void listenWmBasePing(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial);
static void listenSurfaceConfigure(void *data, struct xdg_surface *xdg_surface, uint32_t serial);
static void listenToplevelConfigure(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height,
	struct wl_array *states);
static void listenToplevelClose(void *data, struct xdg_toplevel *xdg_toplevel);
static void listenToplevelConfigureBounds(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height);
static void listenToplevelWmCapabilities(void *data, struct xdg_toplevel *xdg_toplevel, struct wl_array *capabilities);

static struct xdg_wm_base *wmBase;

static struct wl_registry_listener registryListener = {
	.global = listenRegistryGlobal,
	.global_remove = listenRegistryGlobalRemove
};
static struct xdg_wm_base_listener wmBaseListener = {
	.ping = listenWmBasePing
};
static struct xdg_surface_listener surfaceListener = {
	.configure = listenSurfaceConfigure
};
static struct xdg_toplevel_listener toplevelListener = {
	.configure = listenToplevelConfigure,
	.close = listenToplevelClose,
	.configure_bounds = listenToplevelConfigureBounds,
	.wm_capabilities = listenToplevelWmCapabilities
};

XdgShell *XdgShell_Create(struct wl_display *display, struct wl_surface *surface, const char *title)
{
	XdgShell *shell = NULL;

	shell = (XdgShell *)malloc(sizeof (XdgShell));
	assert(shell);

	shell->registry = wl_display_get_registry(display);;
	wl_registry_add_listener(shell->registry, &registryListener, NULL);
	wl_display_roundtrip(display);

	shell->surface = xdg_wm_base_get_xdg_surface(wmBase, surface);
	xdg_surface_add_listener(shell->surface, &surfaceListener, NULL);

	shell->toplevel = xdg_surface_get_toplevel(shell->surface);
	xdg_toplevel_add_listener(shell->toplevel, &toplevelListener, NULL);

	xdg_toplevel_set_title(shell->toplevel, title);

	return shell;
}

void XdgShell_Destroy(XdgShell *shell)
{
	wl_registry_destroy(shell->registry);
	xdg_toplevel_destroy(shell->toplevel);
	xdg_surface_destroy(shell->surface);
	free(shell);
}

static void listenRegistryGlobal(void *data, struct wl_registry *wl_registry, uint32_t name, const char *interface,
	uint32_t version)
{
	if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
		wmBase = wl_registry_bind(wl_registry, name, &xdg_wm_base_interface, version);
		xdg_wm_base_add_listener(wmBase, &wmBaseListener, NULL);
	}
}

static void listenRegistryGlobalRemove(void *data, struct wl_registry *wl_registry, uint32_t name)
{}

static void listenWmBasePing(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial)
{
	xdg_wm_base_pong(xdg_wm_base, serial);
}

static void listenSurfaceConfigure(void *data, struct xdg_surface *xdg_surface, uint32_t serial)
{
	xdg_surface_ack_configure(xdg_surface, serial);
}

static void listenToplevelConfigure(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height,
	struct wl_array *states)
{}

static void listenToplevelClose(void *data, struct xdg_toplevel *xdg_toplevel)
{}

static void listenToplevelConfigureBounds(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height)
{}

static void listenToplevelWmCapabilities(void *data, struct xdg_toplevel *xdg_toplevel, struct wl_array *capabilities)
{}
