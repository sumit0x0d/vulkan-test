#ifndef VULKAN_TEST_WAYLAND_XDG_SHELL_H
#define VULKAN_TEST_WAYLAND_XDG_SHELL_H

#include <wayland-client-protocol.h>

#include "xdg-shell-client-protocol/xdg-shell-client-protocol.h"

typedef struct {
	struct wl_registry *registry;
	struct xdg_surface *surface;
	struct xdg_toplevel *toplevel;
} XdgShell;

XdgShell *XdgShell_Create(struct wl_display *display, struct wl_surface *surface, const char *title);
void XdgShell_Destroy(XdgShell *shell);

#endif
