#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "vulkan.h"

#include <vulkan/vulkan_core.h>
#include <wayland-client-protocol.h>

#define MAJOR 1
#define MINOR 0
#define PATCH 0

void createVkInstance(Vulkan *vulkan, const char *applicationName);
void createVkPhysicalDevice(Vulkan *vulkan);
void createVkDevice(Vulkan *vulkan);
void createVkSurface(Vulkan *vulkan, struct wl_display *display, struct wl_surface *surface);
void createVkQueue(Vulkan *vulkan);
void createVkSwapchain(Vulkan *vulkan, uint32_t width, uint32_t height);
void createVkImage(Vulkan *vulkan, uint32_t width, uint32_t height);
void createVkCommandPool(Vulkan *vulkan);
void createVkImageView(Vulkan *vulkan);
void createVkSemaphore(Vulkan *vulkan);

Vulkan *Vulkan_Create(const char *applicationName, struct wl_display *display, struct wl_surface *surface,
	uint32_t width, uint32_t height)
{
	Vulkan *vulkan = NULL;
	VkResult result = 0;

	vulkan = (Vulkan *)malloc(sizeof (Vulkan));
	assert(vulkan);

	createVkInstance(vulkan, applicationName);
	createVkPhysicalDevice(vulkan);
	createVkDevice(vulkan);
	createVkSurface(vulkan, display, surface);
	createVkSwapchain(vulkan, width, height);
	createVkImage(vulkan, width, height);
	createVkCommandPool(vulkan);
	createVkSemaphore(vulkan);
	render(vulkan);
	// createVkImageView(vulkan);

	return vulkan;
}

void Vulkan_Destroy(Vulkan *vulkan)
{
	vkDestroySwapchainKHR(vulkan->device, vulkan->swapchain, NULL);
	vkDestroySurfaceKHR(vulkan->instance, vulkan->surface, NULL);
	vkDestroyDevice(vulkan->device, NULL);
	vkDestroyInstance(vulkan->instance, NULL);
	free(vulkan);
}

void createVkInstance(Vulkan *vulkan, const char *applicationName)
{
	VkApplicationInfo applicationInfo = {0};
	VkInstanceCreateInfo instanceCreateInfo = {0};
	const char* layerNames[] = {
		"VK_LAYER_MESA_device_select",
		"VK_LAYER_KHRONOS_validation"
	};
	const char* extensionNames[] = {
		VK_KHR_SURFACE_EXTENSION_NAME,
		VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME
	};
	VkResult result = 0;

	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pApplicationName = applicationName;
	applicationInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);

	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pApplicationInfo = &applicationInfo;
	instanceCreateInfo.enabledLayerCount = 2;
	instanceCreateInfo.ppEnabledLayerNames = layerNames;
	instanceCreateInfo.enabledExtensionCount = 2;
	instanceCreateInfo.ppEnabledExtensionNames = extensionNames;

	result = vkCreateInstance(&instanceCreateInfo, NULL, &vulkan->instance);
	assert(result == 0);
}

void createVkPhysicalDevice(Vulkan *vulkan)
{
	VkResult result = 0;
	VkPhysicalDeviceProperties deviceProperties = {0};

	result = vkEnumeratePhysicalDevices(vulkan->instance, &vulkan->physicalDeviceCount, NULL);
	assert(result == 0);
	vulkan->physicalDevices = (VkPhysicalDevice *)malloc(vulkan->physicalDeviceCount * sizeof (VkPhysicalDevice));
	assert(vulkan->physicalDevices);
	result = vkEnumeratePhysicalDevices(vulkan->instance, &vulkan->physicalDeviceCount, vulkan->physicalDevices);
	assert(result == 0);

	for (int i = 0; i < vulkan->physicalDeviceCount; i++) {
		vkGetPhysicalDeviceProperties(vulkan->physicalDevices[i], &deviceProperties);
		if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU) {
			vulkan->physicalDevice = vulkan->physicalDevices[i];
			vulkan->queueFamilyIndex = i;
			break;
		}
	}
}

void createVkDevice(Vulkan *vulkan)
{
	VkDeviceQueueCreateInfo deviceQueueCreateInfo = {0};
	float queuePriority = 1.0f;
	VkDeviceCreateInfo deviceCreateInfo = {0};
	const char *extensionNames[] = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};
	VkResult result = 0;

	deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueCreateInfo.queueFamilyIndex = vulkan->queueFamilyIndex;
	deviceQueueCreateInfo.queueCount = 1;
	deviceQueueCreateInfo.pQueuePriorities = &queuePriority;

	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.enabledExtensionCount = 1;
	deviceCreateInfo.ppEnabledExtensionNames = extensionNames;

	result = vkCreateDevice(vulkan->physicalDevice, &deviceCreateInfo, NULL,
		&vulkan->device);
	assert(result == 0);

	vkGetDeviceQueue(vulkan->device, vulkan->queueFamilyIndex, 0, &vulkan->queue);
}

void createVkSurface(Vulkan *vulkan, struct wl_display *display, struct wl_surface *surface)
{
	VkWaylandSurfaceCreateInfoKHR waylandSurfaceCreateInfo = {0};
	VkResult result = 0;

	waylandSurfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR;
	waylandSurfaceCreateInfo.display = display;
	waylandSurfaceCreateInfo.surface = surface;
	result = vkCreateWaylandSurfaceKHR(vulkan->instance, &waylandSurfaceCreateInfo, NULL, &vulkan->surface);
	assert(result == 0);
}

void createVkSwapchain(Vulkan *vulkan, uint32_t width, uint32_t height)
{
	VkResult result = 0;
	VkSurfaceCapabilitiesKHR surfaceCapabilities = {0};
	uint32_t surfaceFormatCount = 0;
	VkSurfaceFormatKHR *surfaceFormats = NULL;
	uint32_t presentModeCount = 0;
	VkPresentModeKHR *presentModes = NULL;
	VkSwapchainCreateInfoKHR swapchainCreateInfo = {0};

	result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vulkan->physicalDevice, vulkan->surface,
		&surfaceCapabilities);
	assert(result == 0);

	result = vkGetPhysicalDeviceSurfaceFormatsKHR(vulkan->physicalDevice, vulkan->surface, &surfaceFormatCount,
		NULL);
	assert(result == 0);
	surfaceFormats = (VkSurfaceFormatKHR *)malloc(surfaceFormatCount * sizeof (VkSurfaceFormatKHR));
	assert(surfaceFormats);
	result = vkGetPhysicalDeviceSurfaceFormatsKHR(vulkan->physicalDevice, vulkan->surface, &surfaceFormatCount,
		surfaceFormats);
	assert(result == 0);
	vkGetPhysicalDeviceSurfacePresentModesKHR(vulkan->physicalDevice, vulkan->surface, &presentModeCount, NULL);
	assert(result == 0);
	presentModes = (VkPresentModeKHR *)malloc(presentModeCount * sizeof (VkPresentModeKHR));
	assert(presentModes);
	vkGetPhysicalDeviceSurfacePresentModesKHR(vulkan->physicalDevice, vulkan->surface, &presentModeCount, presentModes);
	assert(result == 0);

	swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchainCreateInfo.surface = vulkan->surface;
	swapchainCreateInfo.preTransform = surfaceCapabilities.currentTransform;
	swapchainCreateInfo.imageExtent.width = width;
	swapchainCreateInfo.imageExtent.height = height;
	swapchainCreateInfo.minImageCount = surfaceCapabilities.minImageCount;

	for (uint32_t i = 0; i < surfaceFormatCount; i++) {
		if (surfaceFormats[i].format == VK_FORMAT_B8G8R8A8_SRGB) {
			swapchainCreateInfo.imageFormat = VK_FORMAT_B8G8R8A8_SRGB;
			break;
		} else {}
	}
	swapchainCreateInfo.imageArrayLayers = surfaceCapabilities.maxImageArrayLayers;
	for (uint32_t i = 0; i < presentModeCount; i++) {
		if (presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
			swapchainCreateInfo.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
			break;
		} else if (presentModes[i] == VK_PRESENT_MODE_FIFO_KHR) {
			swapchainCreateInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
			break;
		} else {}
	}

	result = vkCreateSwapchainKHR(vulkan->device, &swapchainCreateInfo, NULL, &vulkan->swapchain);
	assert(result == 0);

	result = vkGetSwapchainImagesKHR(vulkan->device, vulkan->swapchain, &vulkan->swapchainImageCount, NULL);
	assert(result == 0);
	vulkan->swapchainImages = (VkImage *)malloc(vulkan->swapchainImageCount * sizeof (VkImage));
	assert(vulkan->swapchainImages);
	result = vkGetSwapchainImagesKHR(vulkan->device, vulkan->swapchain, &vulkan->swapchainImageCount,
		vulkan->swapchainImages);
	assert(result == 0);

	free(presentModes);
	free(surfaceFormats);
}

void createVkImage(Vulkan *vulkan, uint32_t width, uint32_t height)
{
	VkImageCreateInfo imageCreateInfo = {0};
	VkResult result = 0;

	imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
	imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageCreateInfo.format = VK_FORMAT_B8G8R8A8_SRGB;
	imageCreateInfo.samples = VK_SAMPLE_COUNT_16_BIT;
	imageCreateInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	imageCreateInfo.extent.width = width;
	imageCreateInfo.extent.height = height;
	imageCreateInfo.extent.depth = 1;
	imageCreateInfo.mipLevels = 1;
	imageCreateInfo.arrayLayers = 1;

	result = vkCreateImage(vulkan->device, &imageCreateInfo, NULL, &vulkan->image);
	assert(result == 0);
}

void createVkCommandPool(Vulkan *vulkan)
{
	VkCommandPoolCreateInfo commandPoolCreateInfo = {0};
	VkResult result = 0;

	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.queueFamilyIndex = vulkan->queueFamilyIndex;

	result = vkCreateCommandPool(vulkan->device, &commandPoolCreateInfo, NULL, &vulkan->commandPool);
	assert(result == 0);
}

void createVkImageView(Vulkan *vulkan)
{
	VkImageViewCreateInfo imageViewCreateInfo = {0};
	VkResult result = 0;

	imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	imageViewCreateInfo.format = VK_FORMAT_B8G8R8A8_SRGB;
	imageViewCreateInfo.image = vulkan->image;
	imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imageViewCreateInfo.subresourceRange.layerCount = 1;
	imageViewCreateInfo.subresourceRange.levelCount = 1;
	imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;

	result = vkCreateImageView(vulkan->device, &imageViewCreateInfo, NULL, &vulkan->imageView);
	assert(result == 0);
}

void createVkSemaphore(Vulkan *vulkan)
{
	VkSemaphoreCreateInfo semaphoreCreateInfo = {0};
	
	semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(vulkan->device, &semaphoreCreateInfo, NULL, &vulkan->semaphore);
	vkCreateSemaphore(vulkan->device, &semaphoreCreateInfo, NULL, &vulkan->semaphore1);
}

void render(Vulkan *vulkan)
{
	uint32_t imageIndex = 0;
	VkPipelineStageFlags waitDstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

	vkAcquireNextImageKHR(vulkan->device, vulkan->swapchain, 0, vulkan->semaphore1, 0, &imageIndex);

	VkCommandBuffer commandBuffer;
	VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.commandPool = vulkan->commandPool;
	commandBufferAllocateInfo.commandBufferCount = 1;
	vkAllocateCommandBuffers(vulkan->device, &commandBufferAllocateInfo, &commandBuffer);

	VkCommandBufferBeginInfo commandBufferBeginInfo = {0};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);

	VkClearColorValue color = {1,0,0,1};
	VkImageSubresourceRange range = {};
	range.layerCount = 1;
	range.levelCount = 1;
	range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	vkCmdClearColorImage(commandBuffer, vulkan->swapchainImages[imageIndex], VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, &color, 1, &range);
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo = {0};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = &vulkan->semaphore;
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = &vulkan->semaphore1;
	submitInfo.pWaitDstStageMask = &waitDstStageMask;
	vkQueueSubmit(vulkan->queue, 1, &submitInfo, 0);
	
	VkPresentInfoKHR presentInfo = {0};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.pSwapchains = &vulkan->swapchain;
	presentInfo.swapchainCount = 1;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pWaitSemaphores = &vulkan->semaphore;
	presentInfo.waitSemaphoreCount = 1;
	vkQueuePresentKHR(vulkan->queue, &presentInfo);

	vkDeviceWaitIdle(vulkan->device);
	vkFreeCommandBuffers(vulkan->device, vulkan->commandPool, 1, &commandBuffer);
}
