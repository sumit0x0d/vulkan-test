#ifndef VULKAN_TEST_VULKAN_H
#define VULKAN_TEST_VULKAN_H

#define VK_USE_PLATFORM_WAYLAND_KHR
#define VK_ENABLE_BETA_EXTENSIONS

#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

typedef struct {
	VkInstance instance;
	uint32_t physicalDeviceCount;
	VkPhysicalDevice *physicalDevices;
	VkPhysicalDevice physicalDevice;
	uint32_t queueFamilyIndex;
	VkDevice device;
	VkSurfaceKHR surface;
	VkQueue queue;
	VkSwapchainKHR swapchain;
	uint32_t swapchainImageCount;
	VkImage *swapchainImages;
	VkImageView imageView;
	VkImage image;
	VkCommandPool commandPool;
	VkSemaphore semaphore;
	VkSemaphore semaphore1;
} Vulkan;

Vulkan *Vulkan_Create(const char *applicationName, struct wl_display *display, struct wl_surface *surface,
	uint32_t width, uint32_t height);
void Vulkan_Destroy(Vulkan *vulkan);
void render(Vulkan *vulkan);

#endif
