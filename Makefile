CC = gcc

CFLAGS = -g # -Wall -Wpedantic -Wextra

all:
	$(CC) main.c -o vulkan-test \
	vulkan/vulkan.c \
	wayland/wayland.c \
	wayland/xdg-shell/xdg-shell.c \
	wayland/xdg-shell/xdg-shell-client-protocol/xdg-shell-client-protocol.c \
	$(CFLAGS) -lvulkan -lwayland-client

clean:
	rm vulkan-test
