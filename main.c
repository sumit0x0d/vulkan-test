#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <wayland-client-protocol.h>
#include <wayland-util.h>

#include "wayland/wayland.h"
#include "vulkan/vulkan.h"

#define WIDTH 1920
#define HEIGHT 1080
#define FPS 60

int main()
{
	FILE *file = fopen("../buffer1", "r");
	assert(file);

	void *jpegBuffer = malloc(1920 * 1080 * 4);
	assert(jpegBuffer);

	FILE *file2 = fopen("../buffer2", "r");
	assert(file2);

	void *jpegBuffer2 = malloc(1920 * 1080 * 4);
	assert(jpegBuffer2);

	size_t size1 = fread(jpegBuffer, 1, 1920 * 1080 * 4, file);
	size_t size2 = fread(jpegBuffer2, 1, 1920 * 1080 * 4, file2);

	const char *title = "Vulkan Test";

	Wayland *wayland = Wayland_Create(WIDTH, HEIGHT, title);
	Vulkan *vulkan = Vulkan_Create(title, wayland->display, wayland->surface, WIDTH, HEIGHT);

	int c = 0;
	struct timespec ts1, ts2;
	unsigned long frameTime = 1000000/FPS;

#if 1
	while (wl_display_dispatch(wayland->display) != -1) {
		// clock_gettime(1, &ts1);
		// if (c % 2 == 0) {
		// 	memcpy(wayland->shm, jpegBuffer, size1);
		// } else {
		// 	memcpy(wayland->shm, jpegBuffer2, size2);
		// }
		// wl_surface_attach(wayland->surface, wayland->buffer, 0, 0);
		// wl_surface_damage(wayland->surface, 0, 0, WIDTH, HEIGHT);
		// wl_surface_commit(wayland->surface);
		// clock_gettime(1, &ts2);
		render(vulkan);

		// unsigned long sec = ts2.tv_sec - ts1.tv_sec;
		// unsigned long nsec = (ts2.tv_nsec + (sec * 1e9)) - ts1.tv_nsec;
		// unsigned long usec = nsec/1000;
		// long stime = frameTime - usec;
		// if (stime > 0) {
		// 	usleep(stime);
		// }
		// c++;
	}
#endif

	return EXIT_SUCCESS;
}
